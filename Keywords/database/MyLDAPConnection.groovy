package database

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.naming.Context
import javax.naming.NamingEnumeration
import javax.naming.directory.DirContext
import javax.naming.directory.InitialDirContext
import javax.naming.directory.SearchControls
import javax.naming.directory.SearchResult
import org.apache.directory.api.ldap.model.cursor.SearchCursor
import org.apache.directory.api.ldap.model.entry.DefaultAttribute
import org.apache.directory.api.ldap.model.entry.Entry
import org.apache.directory.api.ldap.model.message.Response
import org.apache.directory.api.ldap.model.message.SearchRequest
import org.apache.directory.api.ldap.model.message.SearchRequestImpl
import org.apache.directory.api.ldap.model.message.SearchResultEntry
import org.apache.directory.api.ldap.model.message.SearchScope
import org.apache.directory.api.ldap.model.name.Dn
import org.apache.directory.ldap.client.api.LdapConnection
import org.apache.directory.ldap.client.api.LdapNetworkConnection
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable


public class MyLDAPConnection {

	public static LdapConnection connection;

	@Keyword
	def connectLDAP(String host,int port){
		connection = new LdapNetworkConnection( host, port );
	}

	@Keyword
	def authentication(String uid, String password){
		try{
			connection.bind( "uid="+uid+",ou=People,dc=aprdev,dc=com", password );
		}catch(Exception e){
			KeywordUtil.markFailed("Authentication failed")
		}
		finally{
			KeywordUtil.markPassed("Authentication success")
		}
	}

	@Keyword
	def checkUser(String uid) throws Exception {

		def req = new SearchRequestImpl()
		req.setScope(SearchScope.SUBTREE)
		req.addAttributes("*")
		req.setTimeLimit(0)
		req.setBase( new Dn("ou=People,dc=aprdev,dc=com"))
		req.setFilter("(uid="+uid+")")

		def result = new ArrayList();

		def searchCursor = connection.search(req)
		while (searchCursor.next())	{
			def response = searchCursor.get()


			if (response instanceof SearchResultEntry) {
				def resultEntry = ((SearchResultEntry)response).getEntry()
				result.add(resultEntry)
				def uidValue = resultEntry.get("uid").getString()
				println uidValue
			}
		}
		searchCursor.close()

		if(result.size!=0){
			KeywordUtil.markPassed("User is exist")
		}else{
			KeywordUtil.markFailed("User is not exist")
		}
	}

	@Keyword
	def closeLDAP(){
		connection.close();
	}
}
