package get

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;



import internal.GlobalVariable

public class CurrentDateTime {
	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	@Keyword
	public void getCurrentDateTime() {

		Locale id = new Locale("in", "ID");
		//System.out.println("Current Date is : " + now.get(Calendar.DATE));

		//String indonesianDate =  suratKeluarc.TglSurat.ToString();

		// Parse the date string using the indonesian cultureinfo
		//System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("id-ID");
		//DateTime dt = DateTime.Parse(indonesianDate, cultureinfo);
		//	lblTglSuratKeluar.Text = suratKeluarc.TglSurat.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));

		// Get your formatted string.
		//lblTglSuratKeluar.Text = dt.ToString("dd MMMM yyyy", cultureinfo);

		LocalDate localDate = LocalDate.now();
		def date = DateTimeFormatter.ofPattern("dd").format(localDate);
		//String NewDate = date.replace('/', ' ')


		System.out.print(date)
	}
}