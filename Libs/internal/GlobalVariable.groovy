package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object url
     
    /**
     * <p></p>
     */
    public static Object jumlah_pinjam_L0
     
    /**
     * <p></p>
     */
    public static Object durasi_pinjam_L0
     
    /**
     * <p></p>
     */
    public static Object tgl_pengajuan_L0
     
    /**
     * <p></p>
     */
    public static Object biaya_layanan_L0
     
    /**
     * <p></p>
     */
    public static Object jatuh_tempo_L0
     
    /**
     * <p></p>
     */
    public static Object jumlah_pembayaran_L0
     
    /**
     * <p></p>
     */
    public static Object txt_nama
     
    /**
     * <p></p>
     */
    public static Object tgl_lahir
     
    /**
     * <p></p>
     */
    public static Object txt_tempat_lahir
     
    /**
     * <p></p>
     */
    public static Object usia
     
    /**
     * <p></p>
     */
    public static Object no_telp_hp
     
    /**
     * <p></p>
     */
    public static Object no_telp_tempat_tinggal
     
    /**
     * <p></p>
     */
    public static Object txt_email
     
    /**
     * <p></p>
     */
    public static Object no_ktp
     
    /**
     * <p></p>
     */
    public static Object nama_akun
     
    /**
     * <p></p>
     */
    public static Object no_rek
     
    /**
     * <p></p>
     */
    public static Object nama_bank
     
    /**
     * <p></p>
     */
    public static Object txt_loan_id
     
    /**
     * <p></p>
     */
    public static Object Logout
     
    /**
     * <p></p>
     */
    public static Object txt_brw_id
     
    /**
     * <p></p>
     */
    public static Object txt_executor
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['url' : 'https://staging.uangteman.com/', 'jumlah_pinjam_L0' : '', 'durasi_pinjam_L0' : '', 'tgl_pengajuan_L0' : '', 'biaya_layanan_L0' : '', 'jatuh_tempo_L0' : '', 'jumlah_pembayaran_L0' : '', 'txt_nama' : 'ferdinand taslim', 'tgl_lahir' : '23/08/1991', 'txt_tempat_lahir' : 'Jakarta Barat', 'usia' : '', 'no_telp_hp' : '081977915579', 'no_telp_tempat_tinggal' : '0217788990', 'txt_email' : 'ferdinand.taslim@uangteman.com', 'no_ktp' : '8438493894389483', 'nama_akun' : 'ferdinand taslim', 'no_rek' : '892839893829399', 'nama_bank' : 'BCA', 'txt_loan_id' : '', 'Logout' : 'https://staging.uangteman.com/admin/logout', 'txt_brw_id' : ''])
        allVariables.put('Ferdinand', ['txt_executor' : 'Ferdinand'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        def selectedVariables = allVariables[profileName]
		
		for(object in selectedVariables){
			String overridingGlobalVariable = RunConfiguration.getOverridingGlobalVariable(object.key)
			if(overridingGlobalVariable != null){
				selectedVariables.put(object.key, overridingGlobalVariable)
			}
		}

        url = selectedVariables["url"]
        jumlah_pinjam_L0 = selectedVariables["jumlah_pinjam_L0"]
        durasi_pinjam_L0 = selectedVariables["durasi_pinjam_L0"]
        tgl_pengajuan_L0 = selectedVariables["tgl_pengajuan_L0"]
        biaya_layanan_L0 = selectedVariables["biaya_layanan_L0"]
        jatuh_tempo_L0 = selectedVariables["jatuh_tempo_L0"]
        jumlah_pembayaran_L0 = selectedVariables["jumlah_pembayaran_L0"]
        txt_nama = selectedVariables["txt_nama"]
        tgl_lahir = selectedVariables["tgl_lahir"]
        txt_tempat_lahir = selectedVariables["txt_tempat_lahir"]
        usia = selectedVariables["usia"]
        no_telp_hp = selectedVariables["no_telp_hp"]
        no_telp_tempat_tinggal = selectedVariables["no_telp_tempat_tinggal"]
        txt_email = selectedVariables["txt_email"]
        no_ktp = selectedVariables["no_ktp"]
        nama_akun = selectedVariables["nama_akun"]
        no_rek = selectedVariables["no_rek"]
        nama_bank = selectedVariables["nama_bank"]
        txt_loan_id = selectedVariables["txt_loan_id"]
        Logout = selectedVariables["Logout"]
        txt_brw_id = selectedVariables["txt_brw_id"]
        txt_executor = selectedVariables["txt_executor"]
        
    }
}
