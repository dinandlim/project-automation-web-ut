<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Step 1 Logo</name>
   <tag></tag>
   <elementGuidId>37947c7e-d2e7-4785-a29e-9e91d0987d3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id=&quot;agreement-form&quot;]/div/div/p[text()=&quot;1&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
