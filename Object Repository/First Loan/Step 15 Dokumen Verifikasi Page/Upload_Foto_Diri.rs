<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload_Foto_Diri</name>
   <tag></tag>
   <elementGuidId>f3b54ccf-1431-4398-a326-24fbc158007f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'uploadBtnOther_3']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id=&quot;uploaderContainerOther_3&quot;]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>uploadBtnOther_3</value>
   </webElementProperties>
</WebElementEntity>
