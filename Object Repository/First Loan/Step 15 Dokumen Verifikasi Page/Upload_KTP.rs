<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload_KTP</name>
   <tag></tag>
   <elementGuidId>444c71d1-601a-44a3-90ba-4020133a41c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'uploadBtnOther_1']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id=&quot;uploaderContainerOther_1&quot;]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>uploadBtnOther_1</value>
   </webElementProperties>
</WebElementEntity>
