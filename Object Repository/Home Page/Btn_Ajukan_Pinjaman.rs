<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Ajukan_Pinjaman</name>
   <tag></tag>
   <elementGuidId>98e6fab8-5148-4728-8339-092e9dbaf5d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'btnAjukanPinjaman']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id=&quot;pinjaman-online&quot;]/div/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnAjukanPinjaman</value>
   </webElementProperties>
</WebElementEntity>
