<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Title_Login_Page</name>
   <tag></tag>
   <elementGuidId>60e37aa3-bb40-4331-af59-d840b77e48ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id=&quot;login-user&quot;]/h3[contains(.,&quot;Login ke Akun Kamu&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
