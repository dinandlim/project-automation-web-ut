import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Loan Calculator Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Jumlah_Pinjam = data.getValue(1, index)

    Durasi_Pinjam = data.getValue(2, index)

    Integer int_JumlahPinjaman = Integer.parseInt(Jumlah_Pinjam)

    Integer int_DurasiPinjaman = Integer.parseInt(Durasi_Pinjam)

    WebUI.waitForElementVisible(findTestObject('First Loan/Loan Calculator Page/Btn_Pinjam_Sekarang'), 0)

    //if (int_JumlahPinjaman >= 1000000 && int_JumlahPinjaman <=3000000){
    //	WebUI.dragAndDropByOffset(findTestObject('Object Repository/CALCULATOR_PAGE/Calc_Jml_Pinjaman'), -500, 0)
    //	WebUI.delay(3)
    if (int_JumlahPinjaman >= 1000000) {
        //Integer TotalDrag = ((int_JumlahPinjaman - 1000000 )/ 100000)*20
        Integer TotalDragJumlahPinjaman = (int_JumlahPinjaman - 1000000) / 100000

        Integer SliderJumlahPinjaman = 5 * TotalDragJumlahPinjaman

        CustomKeywords.'set.ElementApr.addAttribute'(findTestObject('First Loan/Loan Calculator Page/Btn_Slider_Jumlah_Pinjaman'), 
            'style', ('left:' + SliderJumlahPinjaman) + '%;')

        //for (def index2 : (1..TotalDrag)){
        //WebUI.dragAndDropByOffset(findTestObject('Object Repository/CALCULATOR_PAGE/Calc_Jml_Pinjaman'), 20, 0)
        //	CustomKeywords.'set.ElementApr.addAttribute'(findTestObject('First Loan/Loan Calculator Page/Btn_Slider_Jumlah_Pinjaman'), 'style', 'left: 5%;')
        //}
        WebUI.click(findTestObject('First Loan/Loan Calculator Page/Btn_Slider_Jumlah_Pinjaman'))
    }
    
    if (int_DurasiPinjaman >= 10) {
        Integer TotalDragDurasiPinjaman = int_DurasiPinjaman - 10

        Integer SliderJumlahDurasi = 5 * TotalDragDurasiPinjaman

        CustomKeywords.'set.ElementApr.addAttribute'(findTestObject('First Loan/Loan Calculator Page/Btn_Slider_Durasi_Pinjaman'), 
            'style', ('left:' + SliderJumlahDurasi) + '%;')

        WebUI.click(findTestObject('First Loan/Loan Calculator Page/Btn_Slider_Durasi_Pinjaman'))
    }
    
    //CustomKeywords.'set.ElementApr.addAttribute'(findTestObject('First Loan/Loan Calculator Page/Btn_Slider_Jumlah_Pinjaman'), 'style', 'left: 5%;')
    //WebUI.click(findTestObject('First Loan/Loan Calculator Page/Btn_Slider_Jumlah_Pinjaman'))
    CustomKeywords.'get.Screencapture.getScreenShot'('Loan Calculator.jpg')

    GlobalVariable.jumlah_pinjam_L0 = WebUI.getText(findTestObject('First Loan/Loan Calculator Page/Text_Jumlah_Pinjaman'))

    GlobalVariable.durasi_pinjam_L0 = WebUI.getText(findTestObject('First Loan/Loan Calculator Page/Text_Durasi_Pinjaman'))

    GlobalVariable.biaya_layanan_L0 = WebUI.getText(findTestObject('First Loan/Loan Calculator Page/Text_Biaya_Layanan'))

    GlobalVariable.jumlah_pembayaran_L0 = WebUI.getText(findTestObject('First Loan/Loan Calculator Page/Text_Jumlah_Pembayaran'))

    GlobalVariable.jatuh_tempo_L0 = WebUI.getText(findTestObject('First Loan/Loan Calculator Page/Text_Jatuh_Tempo'))
}

WebUI.waitForElementVisible(findTestObject('First Loan/Loan Calculator Page/Btn_Pinjam_Sekarang'), 0)

WebUI.waitForElementClickable(findTestObject('First Loan/Loan Calculator Page/Btn_Pinjam_Sekarang'), 0)

WebUI.click(findTestObject('First Loan/Loan Calculator Page/Btn_Pinjam_Sekarang'))

