import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 10 Status Rumah Tinggal Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Status_Rumah_Tinggal = data.getValue(1, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/Logo Step 10'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/List_Kontrak'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/List_Kos'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/List_Rumah_Dinas'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/List_Rumah_Orang_Tua'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/List_Rumah_Sendiri'), 0)

    WebUI.scrollToElement(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/Logo Step 10'), 0)

    lbl_Status_Rumah_Tinggal = new TestObject('')

    lbl_Status_Rumah_Tinggal.addProperty('xpath', ConditionType.EQUALS, ('//form[@id=\'step9\']/div/div[2]/div/div[' + Status_Rumah_Tinggal) + 
        ']/div[2]/label/div/span')

    WebUI.waitForElementClickable(lbl_Status_Rumah_Tinggal, 0)

    WebUI.click(lbl_Status_Rumah_Tinggal) //WebUI.waitForElementClickable(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/List_Rumah_Sendiri'), 0)
    //WebUI.click(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/List_Rumah_Sendiri'), FailureHandling.STOP_ON_FAILURE)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-10.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 10 Status Rumah Tinggal Page/Btn_Selanjutnya'))

