import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 11 Nama dan Alamat Keluarga Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Keluarga = data.getValue(1, index)

    No_Tlp_Keluarga = data.getValue(2, index)

    Alamat_Keluarga = data.getValue(3, index)

    Provinsi_Keluarga = data.getValue(4, index)

    Kota_Keluarga = data.getValue(5, index)

    Kecamatan_Keluarga = data.getValue(6, index)

    Kelurahan_Keluarga = data.getValue(7, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Logo Step 11'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Textfield_Nama_Keluarga'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Textfield_NoTelp_Keluarga'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Textfield_Alamat_Keluarga'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Provinsi_Keluarga'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Kota_Keluarga'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Kecamatan_Keluarga'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Kelurahan_Keluarga'), 
        0)

    WebUI.setText(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Textfield_Nama_Keluarga'), Nama_Keluarga)

    WebUI.setText(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Textfield_NoTelp_Keluarga'), No_Tlp_Keluarga)

    WebUI.setText(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Textfield_Alamat_Keluarga'), Alamat_Keluarga)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Provinsi_Keluarga'), Provinsi_Keluarga, 
        false)

    Dropdown_KotaKeluarga = new TestObject('')

    Dropdown_KotaKeluarga.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_fam1_kab_kot\']/option[@value=\'' + 
        Kota_Keluarga) + '\']')

    WebUI.waitForElementClickable(Dropdown_KotaKeluarga, 0)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Kota_Keluarga'), Kota_Keluarga, 
        false)

    Dropdown_KecamatanKeluarga = new TestObject('')

    Dropdown_KecamatanKeluarga.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_fam1_kecamatan\']/option[@value=\'' + 
        Kecamatan_Keluarga) + '\']')

    WebUI.waitForElementClickable(Dropdown_KecamatanKeluarga, 0)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Kecamatan_Keluarga'), 
        Kecamatan_Keluarga, false)

    Dropdown_KelurahanKeluarga = new TestObject('')

    Dropdown_KelurahanKeluarga.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_fam1_kelurahan\']/option[@value=\'' + 
        Kelurahan_Keluarga) + '\']')

    WebUI.waitForElementClickable(Dropdown_KelurahanKeluarga, 0)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Dropdown_List_Kelurahan_Keluarga'), 
        Kelurahan_Keluarga, false)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-11.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 11 Nama dan Alamat Keluarga Page/Btn_Selanjutnya'))

