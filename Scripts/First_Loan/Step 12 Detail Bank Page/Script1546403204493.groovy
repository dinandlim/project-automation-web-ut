import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 12 Detail Bank Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Akun = data.getValue(1, index)

    Nama_Bank = data.getValue(2, index)

    No_Rekening = data.getValue(3, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 12 Detail Bank Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 12 Detail Bank Page/Logo Step 12'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 12 Detail Bank Page/Textfield_Nama_Pemilik_Akun'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 12 Detail Bank Page/Dropdown_List_Nama_Bank'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 12 Detail Bank Page/Textfield_No_Rekening'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 12 Detail Bank Page/Checkbox_Konfirmasi'), 0)

    WebUI.setText(findTestObject('First Loan/Step 12 Detail Bank Page/Textfield_Nama_Pemilik_Akun'), Nama_Akun)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 12 Detail Bank Page/Dropdown_List_Nama_Bank'), Nama_Bank, false)

    WebUI.setText(findTestObject('First Loan/Step 12 Detail Bank Page/Textfield_No_Rekening'), No_Rekening)

    WebUI.click(findTestObject('First Loan/Step 12 Detail Bank Page/Checkbox_Konfirmasi'))
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-12.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 12 Detail Bank Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 12 Detail Bank Page/Btn_Selanjutnya'))

WebUI.verifyElementNotInViewport(findTestObject('First Loan/Step 1 Syarat dan Ketentuan Page/Btn_SnK'), 0)

