import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 13 Pekerjaan Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Kantor = data.getValue(1, index)

    No_Tlp_Kantor = data.getValue(2, index)

    Alamat_Kantor = data.getValue(3, index)

    Provinsi_Kantor = data.getValue(4, index)

    Kota_Kantor = data.getValue(5, index)

    Kecamatan_Kantor = data.getValue(6, index)

    Kelurahan_Kantor = data.getValue(7, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 13 Pekerjaan Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Step 13 Logo'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Textfield_Nama_Kantor'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Textfield_No_Telp_Kantor'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Textfield_Alamat_Kantor'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Provinsi_Kantor'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Kota_Kantor'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Kecamatan_Kantor'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Kelurahan_Kantor'), 0)

    WebUI.setText(findTestObject('First Loan/Step 13 Pekerjaan Page/Textfield_Nama_Kantor'), Nama_Kantor)

    WebUI.setText(findTestObject('First Loan/Step 13 Pekerjaan Page/Textfield_No_Telp_Kantor'), No_Tlp_Kantor)

    WebUI.setText(findTestObject('First Loan/Step 13 Pekerjaan Page/Textfield_Alamat_Kantor'), Alamat_Kantor)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Provinsi_Kantor'), Provinsi_Kantor, false)

    Dropdown_KotaKantor = new TestObject('')

    Dropdown_KotaKantor.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_employer_kab_kot\']/option[@value=\'' + 
        Kota_Kantor) + '\']')

    WebUI.waitForElementClickable(Dropdown_KotaKantor, 0)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Kota_Kantor'), Kota_Kantor, false)

    Dropdown_KecamatanKantor = new TestObject('')

    Dropdown_KecamatanKantor.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_employer_kecamatan\']/option[@value=\'' + 
        Kecamatan_Kantor) + '\']')

    WebUI.waitForElementClickable(Dropdown_KecamatanKantor, 0)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Kecamatan_Kantor'), Kecamatan_Kantor, 
        false)

    Dropdown_KelurahanKantor = new TestObject('')

    Dropdown_KelurahanKantor.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_employer_kelurahan\']/option[@value=\'' + 
        Kelurahan_Kantor) + '\']')

    WebUI.waitForElementClickable(Dropdown_KelurahanKantor, 0)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 13 Pekerjaan Page/Dropdown_List_Kelurahan_Kantor'), Kelurahan_Kantor, 
        false)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-13.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 13 Pekerjaan Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 13 Pekerjaan Page/Btn_Selanjutnya'))

