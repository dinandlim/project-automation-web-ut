import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 14 Jabatan dan Posisi Karir Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Jenis_Pekerjaan = data.getValue(1, index)

    Lama_Tahun_Bekerja = data.getValue(2, index)

    Lama_Bulan_Bekerja = data.getValue(3, index)

    Jabatan = data.getValue(4, index)

    Penghasilan_PerBulan = data.getValue(5, index)

    Pengeluaran_PerBulan = data.getValue(6, index)

    AngsuranKPR = data.getValue(7, index)
	
	JumlahTanggungan = data.getValue(8, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Step 14 Logo'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_Jenis_Pekerjaan'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_Tahun_Bekerja'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_Bulan_Bekerja'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Textfield_Penghasilan'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Textfield_Pengeluaran'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Textfield_Angsuran'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_JumlahTanggungan'), 
        0)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_Jenis_Pekerjaan'), 
        Jenis_Pekerjaan, false)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_Tahun_Bekerja'), 
        Lama_Tahun_Bekerja, false)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_Bulan_Bekerja'), 
        Lama_Bulan_Bekerja, false)

    WebUI.setText(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Textfield_Jabatan'), Jabatan)

    WebUI.setText(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Textfield_Penghasilan'), Penghasilan_PerBulan)

    WebUI.setText(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Textfield_Pengeluaran'), Pengeluaran_PerBulan)

    WebUI.setText(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Textfield_Angsuran'), AngsuranKPR)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Dropdown_List_JumlahTanggungan'), 
        JumlahTanggungan, false)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-14.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 14 Jabatan dan Posisi Karir Page/Btn_Selanjutnya'))

