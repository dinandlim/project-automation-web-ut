import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 15 Dokumen Verifikasi Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Upload_KTP = data.getValue(1, index)

    Upload_Foto_Diri = data.getValue(2, index)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Step 15 Logo'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Upload_KTP'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Upload_Foto_Diri'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Btn_Selanjutnya'), 0)

    WebUI.setText(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Upload_KTP'), Upload_KTP)

    WebUI.setText(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Upload_Foto_Diri'), Upload_Foto_Diri)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-15.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 15 Dokumen Verifikasi Page/Btn_Selanjutnya'))

