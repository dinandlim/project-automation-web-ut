import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 16 Rincian Pinjaman Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Nama_Lengkap = data.getValue(1, index)

    Tanggal_Lahir = data.getValue(2, index)

    Tempat_Lahir = data.getValue(3, index)

    No_Telp_HP = data.getValue(4, index)

    No_Telp_Rumah = data.getValue(5, index)

    Email = data.getValue(6, index)

    No_KTP = data.getValue(7, index)

    Alamat = data.getValue(8, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Step 16 Logo'), 0, FailureHandling.STOP_ON_FAILURE)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Step 16 Logo'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Jumlah_Pinjaman'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Durasi_Pinjaman'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Tgl_Pengajuan'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Biaya_Layanan'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Jatuh_Tempo'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Jumlah_Pembayaran'), 
        0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Btn_Ubah_Pinjaman'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Nama'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Tgl_Lahir'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Tempat_Lahir'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Usia'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_No_Hp'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_No_Telp_Rumah'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Email'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Bank'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_KTP'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Alamat'), 0)

    expected_jumlah_pinjaman = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Jumlah_Pinjaman'))

    expected_durasi_pinjaman = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Durasi_Pinjaman'))

    expected_tgl_pengajuan = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Tgl_Pengajuan'))

    expected_biaya_layanan = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Biaya_Layanan'))

    expected_jatuh_tempo = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Jatuh_Tempo'))

    expected_jumlah_pembayaran = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Rincian Pinjaman/Text_Jumlah_Pembayaran'))

    expected_nama = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Nama'))

    expected_tgl_lahir = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Tgl_Lahir'))

    expected_tempat_lahir = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Tempat_Lahir'))

    expected_usia = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Usia'))

    expected_no_hp = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_No_Hp'))

    expected_no_tempat_tinggal = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_No_Telp_Rumah'))

    expected_email = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Email'))

    expected_bank = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Bank'))

    expected_ktp = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_KTP'))

    expected_alamat = WebUI.getText(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Data Debitur/Text_Alamat'))

    WebUI.verifyMatch(GlobalVariable.jumlah_pinjam_L0, expected_jumlah_pinjaman, false)

    WebUI.verifyMatch(GlobalVariable.durasi_pinjam_L0, expected_durasi_pinjaman, false)

    WebUI.verifyMatch(GlobalVariable.biaya_layanan_L0, expected_biaya_layanan, false)

    WebUI.verifyMatch(GlobalVariable.jatuh_tempo_L0, expected_jatuh_tempo, false)

    WebUI.verifyMatch(GlobalVariable.jumlah_pembayaran_L0, expected_jumlah_pembayaran, false)

    if (Nama_Lengkap.equalsIgnoreCase(expected_nama)) {
        //String Tgl_Lahir = Tanggal_Lahir.replace('-', '/')
        //WebUI.verifyMatch(Tgl_Lahir, expected_tgl_lahir, false)
        WebUI.verifyMatch(Tempat_Lahir, expected_tempat_lahir, false)

        WebUI.verifyMatch(No_Telp_HP, expected_no_hp, false)

        WebUI.verifyMatch(No_Telp_Rumah, expected_no_tempat_tinggal, false)

        WebUI.verifyMatch(Email, expected_email, false)

        WebUI.verifyMatch(No_KTP, expected_ktp, false)

        WebUI.verifyMatch(Alamat, expected_alamat, false)
    } else {
       // print('Failed = Nama Tidak Sama')

        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Nama Tidak Sama')
    }
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-16.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Btn_Saya_Setuju'), 0)

WebUI.click(findTestObject('First Loan/Step 16 Rincian Pinjaman Page/Btn_Saya_Setuju'))

