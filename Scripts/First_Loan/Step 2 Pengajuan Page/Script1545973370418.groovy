import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 2 Pengajuan Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    email_address = data.getValue(1, index)

    no_telp = data.getValue(2, index)

    no_ktp = data.getValue(3, index)
	
	mother_name = data.getValue(4,index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 2 Pengajuan Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 2 Pengajuan Page/Logo Step 2'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_Email'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_NoTelp'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_KTP'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_Mother_Name'), 0)

    WebUI.setText(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_Email'), email_address)

    WebUI.setText(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_NoTelp'), no_telp)

    WebUI.setText(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_KTP'), no_ktp)

    WebUI.setText(findTestObject('First Loan/Step 2 Pengajuan Page/Textfield_Mother_Name'), mother_name)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-2.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 2 Pengajuan Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 2 Pengajuan Page/Btn_Selanjutnya'))

