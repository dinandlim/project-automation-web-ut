import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 3 Alasan Meminjam Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    ALASAN_PINJAM = data.getValue(1, index)

    MODAL_USAHA = data.getValue(2, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 3 Alasan Meminjam Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/Logo Step 3'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Biaya_Menikah'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Lain_Lain'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Liburan'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Membayar_Hutang'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Pertanian'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Perikanan'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Pertambangan'), 0)

   // WebUI.click(findTestObject('First Loan/Step 3 Alasan Meminjam Page/Btn_Lihat_Selengkapnya'))

    //WebUI.waitForElementVisible(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Industri_Pengolahan'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Industri_Pengolahan'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Listrik_Air_Gas'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Konstruksi'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Perdagangan'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Penyediaan_Akomodasi'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Transportasi'), 0)

    //WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Perantara_Keuangan'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Real_Estate'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Administrasi_Pemerintah'), 0)

    //WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Pendidikan'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Jasa_Kesehatan'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Kegiatan_Organisasi'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Jasa_Perorangan'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Badan_Internasional'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Kegiatan_Belum_Jelas'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Sektor_Ekonomi'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Rumah_Tangga'), 0)

    //WebUI.verifyElementPresent(findTestObject('First Loan/Step 3 Alasan Meminjam Page/List/List_Bukan_Lapangan_Usaha'), 0)

    //lbl_AlasanPinjam =new TestObject("lbl_Text_Alasan_Pinjam");
    Integer int_ModalUsaha = Integer.parseInt(MODAL_USAHA)

    if (ALASAN_PINJAM.equals('5')) {
        IndexModalUsaha = (int_ModalUsaha + 3)

        println(IndexModalUsaha)

        lbl_ModalUsaha = new TestObject('')

        lbl_ModalUsaha.addProperty('xpath', ConditionType.EQUALS, "//*[@id='step2']/div/div[2]/div/div[5]/div['" + IndexModalUsaha + "']/div")

        //Choose Radio Button "Alasan Pinjam Di Uang Teman"
        WebUI.click(lbl_ModalUsaha //Choose Radio Button "Alasan Pinjam Di Uang Teman"
            ) //WebUI.click(findTestObject('First Loan/Step 3 Alasan Meminjam Page/Btn_Pilih_Alasan'))
    } else {
        lbl_AlasanPinjam = new TestObject('')

        lbl_AlasanPinjam.addProperty('xpath', ConditionType.EQUALS, ('//*[@id=\'step2\']/div/div[2]/div/div[' + ALASAN_PINJAM) + 
            ']/div[3]/label/div/span')

        WebUI.click(lbl_AlasanPinjam)
    }
}

//WebUI.delay(5)
CustomKeywords.'get.Screencapture.getScreenShot'('Step-3.jpg')

//}
WebUI.waitForElementClickable(findTestObject('First Loan/Step 3 Alasan Meminjam Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 3 Alasan Meminjam Page/Btn_Selanjutnya'))

