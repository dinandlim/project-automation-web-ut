import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testdata.DBData as DBData
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 4 Darimana Tahu UT Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Darimana_Tahu_UT = data.getValue(1, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/Logo Step 4'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Billboard'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Bioskop'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Brosur'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Email'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Facebook'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Google'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Iklan_Banned_App_Mobile'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Iklan_Banned_Web'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Instagram'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Radio'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Referensi_Teman'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_SMS'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Twitter'), 0)

    lbl_DarimanaTahuUT = new TestObject('')

    lbl_DarimanaTahuUT.addProperty('xpath', ConditionType.EQUALS, ('//form[@id=\'step3\']/div/div[2]/div/div[' + Darimana_Tahu_UT) + 
        ']/div[2]/label/div/span')

    //Choose Radio Button
    WebUI.waitForElementVisible(lbl_DarimanaTahuUT, 0)

    WebUI.click(lbl_DarimanaTahuUT)
}

//WebUI.click(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/List_Google'))
CustomKeywords.'get.Screencapture.getScreenShot'('Step-4.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 4 Darimana Tahu UT Page/Btn_Selanjutnya'))

