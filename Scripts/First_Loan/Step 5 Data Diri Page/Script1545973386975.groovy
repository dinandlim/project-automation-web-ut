import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import internal.GlobalVariable as GlobalVariable

int j = 0

DBData data = findTestData('First Loan/Step 5 Data Diri Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
	Nama_Lengkap = data.getValue(1, index)
	
	Tempat_Lahir = data.getValue(2, index)
	
    Tanggal_Lahir = data.getValue(3, index)

    Jenis_Kelamin = data.getValue(4, index)

    Agama = data.getValue(5, index)

    Status_Pernikahan = data.getValue(6, index)

    String string = Tanggal_Lahir

    String[] parts = string.split('-')

    String tanggal = parts[0]

    String bulan = parts[1]

    String tahun = parts[2]

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 5 Data Diri Page/Textfield_Nama'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 5 Data Diri Page/Logo Step 5'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 5 Data Diri Page/Textfield_Nama'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 5 Data Diri Page/Textfield_Tempat_Lahir'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 5 Data Diri Page/Btn_Tanggal_Lahir'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 5 Data Diri Page/Dropdown_List_Jenis_Kelamin'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 5 Data Diri Page/Dropdown_List_Agama'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 5 Data Diri Page/Dropdown_List_Status'), 0)

    WebUI.setText(findTestObject('First Loan/Step 5 Data Diri Page/Textfield_Nama'), Nama_Lengkap )

    WebUI.setText(findTestObject('First Loan/Step 5 Data Diri Page/Textfield_Tempat_Lahir'), Tempat_Lahir)

    WebUI.click(findTestObject('First Loan/Step 5 Data Diri Page/Btn_Tanggal_Lahir'))

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Prev_Year'), 0)

    lbl_Tahun = new TestObject('')

    lbl_Tahun.addProperty('xpath', ConditionType.EQUALS, ('//div[@class=\'datepicker-years\']/table/tbody/tr/td/span[text()=\'' + 
        tahun) + '\']')

    Boolean btnTahun = WebUI.verifyElementVisible(lbl_Tahun, FailureHandling.OPTIONAL)

    //Boolean exist_Btn_Pilih_Year = WebUI.verifyElementVisible(btn_Pilih_Year, FailureHandling.OPTIONAL)
    if (btnTahun == false) {
        //if(WebUI.verifyElementVisible(lbl_Tahun, FailureHandling.OPTIONAL) == null)
        for (btnTahun; btnTahun != true; j++) {
            btnTahun = WebUI.verifyElementVisible(lbl_Tahun, FailureHandling.OPTIONAL)

            if (btnTahun == false) {
                WebUI.click(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Prev_Year')) //Choose Radio Button Years
                //Choose Radio Button Month
                //Choose Radio Button Day
            } else {
                WebUI.waitForElementClickable(lbl_Tahun, 0)

                WebUI.click(lbl_Tahun)

                lbl_bulan = new TestObject('')

                lbl_bulan.addProperty('xpath', ConditionType.EQUALS, ('//div[@class=\'datepicker-months\']/table/tbody/tr/td/span[text()=\'' + 
                    bulan) + '\'] ')

                WebUI.waitForElementClickable(lbl_bulan, 0)

                WebUI.click(lbl_bulan)

                lbl_tanggal = new TestObject('')

                lbl_tanggal.addProperty('xpath', ConditionType.EQUALS, ('//div[@class=\'datepicker-days\']/table/tbody/tr/td[text()=\'' + 
                    tanggal) + '\'] ')

                WebUI.waitForElementClickable(lbl_tanggal, 0)

                WebUI.click(lbl_tanggal)
            }
        }
        //Choose Radio Button Years
        //Choose Radio Button Month
        //Choose Radio Button Day
    } else {
        WebUI.waitForElementClickable(lbl_Tahun, 0)

        WebUI.click(lbl_Tahun)

        lbl_bulan = new TestObject('')

        lbl_bulan.addProperty('xpath', ConditionType.EQUALS, ('//div[@class=\'datepicker-months\']/table/tbody/tr/td/span[text()=\'' + 
            bulan) + '\'] ')

        WebUI.waitForElementClickable(lbl_bulan, 0)

        WebUI.click(lbl_bulan)

        lbl_tanggal = new TestObject('')

        lbl_tanggal.addProperty('xpath', ConditionType.EQUALS, ('//div[@class=\'datepicker-days\']/table/tbody/tr/td[text()=\'' + 
            tanggal) + '\'] ')

        WebUI.waitForElementClickable(lbl_tanggal, 0)

        WebUI.click(lbl_tanggal)
    }
    
    //btn_Pilih_Year.addProperty('xpath', ConditionType.EQUALS, ('//div[3]/table/tbody/tr/td/span[@class=\'year\' and contains(text(),\'' +yearTglLahir) + '\')]')
    //WebUI.waitForElementVisible(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Selected_Year'), 0)
    //WebUI.click(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Selected_Year'))
    //WebUI.waitForElementVisible(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Selected_Month'), 0)
    //WebUI.click(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Selected_Month'))
    //WebUI.waitForElementVisible(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Selected_Day'), 0)
    //WebUI.click(findTestObject('First Loan/Step 5 Data Diri Page/Tanggal/Btn_Selected_Day'))
    WebUI.selectOptionByValue(findTestObject('First Loan/Step 5 Data Diri Page/Dropdown_List_Jenis_Kelamin'), Jenis_Kelamin, false)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 5 Data Diri Page/Dropdown_List_Agama'), Agama, false)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 5 Data Diri Page/Dropdown_List_Status'), Status_Pernikahan, false)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-5.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 5 Data Diri Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 5 Data Diri Page/Btn_Selanjutnya'))

