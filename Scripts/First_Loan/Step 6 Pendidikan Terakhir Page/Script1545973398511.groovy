import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 6 Pendidikan Terakhir Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Pendidikan = data.getValue(1, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/Logo Step 6'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_Diploma1'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_Diploma2'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_Diploma3'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_S1'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_S2'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_S3'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_SD'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_SLTA'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_SLTP'), 0)

    //WebUI.click(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/List_S2'))
    lbl_Pendidikan = new TestObject('')

    lbl_Pendidikan.addProperty('xpath', ConditionType.EQUALS, ('//form[@id=\'step5\']/div/div[2]/div/div[' + Pendidikan) + 
        ']/div[2]/label/div/span')
	

    //Choose Radio Button
    WebUI.click(lbl_Pendidikan)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-6.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 6 Pendidikan Terakhir Page/Btn_Selanjutnya'))

