import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testdata.DBData as DBData
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 7 Suku dan Jumlah Tanggungan Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Suku = data.getValue(1, index)

    Jml_tanggungan = data.getValue(2, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Logo Step 7'), 0)

   // WebUI.verifyElementPresent(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Dropdown_List_Suku'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Dropdown_List_Tanggungan'), 0)

   // WebUI.selectOptionByValue(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Dropdown_List_Suku'), Suku, false)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Dropdown_List_Tanggungan'), Jml_tanggungan, 
        false)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-7.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 7 Suku dan Jumlah Tanggungan Page/Btn_Selanjutnya'))

