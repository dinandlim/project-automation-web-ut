import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('First Loan/Step 9 Alamat Tempat Tinggal Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    Telp_Rumah = data.getValue(1, index)

    Alamat_Tempat_Tinggal = data.getValue(2, index)

    Provinsi = data.getValue(3, index)

    Kota = data.getValue(4, index)

    Kecamatan = data.getValue(5, index)

    Kelurahan = data.getValue(6, index)

    WebUI.waitForElementVisible(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Btn_Selanjutnya'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Logo Step 9'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Textfield_NoTelp'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Textfield_Alamat'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Provinsi'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Kota'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Kecamatan'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Kelurahan'), 0)

    WebUI.setText(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Textfield_NoTelp'), Telp_Rumah)

    WebUI.setText(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Textfield_Alamat'), Alamat_Tempat_Tinggal)

    WebUI.selectOptionByValue(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Provinsi'), Provinsi, 
        false)

    Text_Kota = new TestObject('')

    Text_Kota.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_dom_kab_kot\']/option[@value=\'' + Kota) + 
        '\']')

    WebUI.waitForElementClickable(Text_Kota, 0)

    //WebUI.waitForElementVisible(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Text_Kota'), 0)
    WebUI.selectOptionByValue(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Kota'), Kota, false)

    Text_Kecamatan = new TestObject('')

    Text_Kecamatan.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_dom_kecamatan\']/option[@value=\'' + Kecamatan) + 
        '\']')

    WebUI.waitForElementClickable(Text_Kecamatan, 0)

    //WebUI.waitForElementVisible(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Text_Kecamatan'), 0)
    WebUI.selectOptionByValue(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Kecamatan'), Kecamatan, 
        false)

    Text_Kelurahan = new TestObject('')

    Text_Kelurahan.addProperty('xpath', ConditionType.EQUALS, ('//select[@id=\'ap_dom_kelurahan\']/option[@value=\'' + Kelurahan) + 
        '\']')

    WebUI.waitForElementClickable(Text_Kelurahan, 0)

    //WebUI.waitForElementVisible(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Text_Kelurahan'), 0)
    WebUI.selectOptionByValue(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Dropdown_List_Kelurahan'), Kelurahan, 
        false)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Step-9.jpg')

WebUI.waitForElementClickable(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Btn_Selanjutnya'), 0)

WebUI.click(findTestObject('First Loan/Step 9 Alamat Tempat Tinggal Page/Btn_Selanjutnya'))

