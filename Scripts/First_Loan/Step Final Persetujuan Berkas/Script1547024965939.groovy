import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.mysql.jdbc.Connection
import java.sql.DriverManager
import com.mysql.jdbc.Connection
import com.mysql.jdbc.PreparedStatement

WebUI.waitForElementVisible(findTestObject('First Loan/Step Final Persetujuan Berkas/Btn_Kembali_ke_Home'), 0)

Boolean persetujuan = WebUI.verifyElementVisible(findTestObject('First Loan/Step Final Persetujuan Berkas/Text_Berkas_Diterima'), FailureHandling.OPTIONAL)

if (persetujuan == true) {
    WebUI.verifyElementPresent(findTestObject('First Loan/Step Final Persetujuan Berkas/Text_Berkas_Diterima'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step Final Persetujuan Berkas/Text_Nama_Peminjam'), 0)

    WebUI.verifyElementPresent(findTestObject('First Loan/Step Final Persetujuan Berkas/Text_Loan_id'), 0)

    write_loan_id = WebUI.getText(findTestObject('First Loan/Step Final Persetujuan Berkas/Text_Loan_id'))
	
	String driver="com.mysql.jdbc.Driver";
	String url="jdbc:mysql://103.58.100.148:3305/utqa2015";
	String uname="ferdinand";
	String pass="?!_C?21h|Z=0J^a7-Zp6)k9^*&2*I2@B%9+I%8?_6";
	Class.forName(driver);
	Connection con = DriverManager.getConnection(url,uname,pass);
	con.getConnectTimeout()
	PreparedStatement ps=con.prepareStatement("UPDATE ut_workbench SET TEMP_LOAN_ID = (?) WHERE TC_ID = 'TC_L0' and EXECUTOR = '"+GlobalVariable.txt_executor+"' ");
	ps.setString(1, write_loan_id);
	   int j =ps.executeUpdate();
	   if(j==1)
	   {
	   System.out.println("data inserted");
	   }
	   else
	   {
	   System.out.println("not inserted");
	   }
	   
	
} else {
    print('Permohonan Tidak Disetujui')

    WebUI.verifyElementPresent(findTestObject('First Loan/Step Final Persetujuan Berkas/Text_Berkas_Diterima'), 0)
}

CustomKeywords.'get.Screencapture.getScreenShot'('Last_Step.jpg')

