import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Home Page/Btn_Login'), 0)

WebUI.verifyElementPresent(findTestObject('Home Page/Home_Logo_UT'), 0)

WebUI.verifyElementPresent(findTestObject('Home Page/Btn_Login'), 0)

WebUI.verifyElementPresent(findTestObject('Home Page/Btn_Ajukan_Pinjaman'), 0)

CustomKeywords.'get.Screencapture.getScreenShot'('Home_Ln.jpg')

WebUI.click(findTestObject('Home Page/Btn_Login'))

