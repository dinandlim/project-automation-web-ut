import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.mysql.jdbc.Connection as Connection
import java.sql.DriverManager as DriverManager
import com.mysql.jdbc.PreparedStatement as PreparedStatement
import com.google.guava19.common.base.Charsets as Charsets
import com.google.guava19.common.hash.Hashing as Hashing

WebUI.callTestCase(findTestCase('Ln/Brw Id Connection'), [:], FailureHandling.STOP_ON_FAILURE)

String driver = 'com.mysql.jdbc.Driver'

String url = 'jdbc:mysql://103.58.100.148:3305/utstag2015new'

String uname = 'ferdinand'

String pass = '?!_C?21h|Z=0J^a7-Zp6)k9^*&2*I2@B%9+I%8?_6'

Class.forName(driver)

Connection con = DriverManager.getConnection(url, uname, pass)

con.getConnectTimeout()

PreparedStatement ps = con.prepareStatement(('UPDATE customer_primary_data SET cd_passwd = ? WHERE cd_brw_id = \'' + GlobalVariable.txt_brw_id) + 
    '\' ')

ps.setString(1, Hashing.sha1().hashString('testing', Charsets.UTF_8).toString())

int j = ps.executeUpdate()

if (j == 1) {
    System.out.println('data inserted')
} else {
    System.out.println('not inserted')
}

DBData data = findTestData('Ln/Login Page')

String executor = GlobalVariable.txt_executor

def lbl_executor = ('\'' + executor) + '\''

data.query = data.query.replace('\'_EXECUTOR_\'', lbl_executor)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    email = data.getValue(1, index)

    password = data.getValue(2, index)

    WebUI.waitForElementVisible(findTestObject('Ln/Login Page/Title_Login_Page'), 0)

    WebUI.verifyElementPresent(findTestObject('Ln/Login Page/Title_Login_Page'), 0)

    WebUI.verifyElementPresent(findTestObject('Ln/Login Page/Textfield_Username'), 0)

    WebUI.verifyElementPresent(findTestObject('Ln/Login Page/Textfield_Password'), 0)

    WebUI.verifyElementPresent(findTestObject('Ln/Login Page/Btn_Login'), 0)

    WebUI.setText(findTestObject('Ln/Login Page/Textfield_Username'), email)

    WebUI.setText(findTestObject('Ln/Login Page/Textfield_Password'), password)

    WebUI.click(findTestObject('Ln/Login Page/Btn_Login'), FailureHandling.STOP_ON_FAILURE)
}

boolean LoginGagal = WebUI.verifyElementPresent(findTestObject('Ln/Login Page/Text_Login_Gagal'), 0, FailureHandling.OPTIONAL)

if (LoginGagal == true) {
    CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Login Failed.. Please check DB')
} else {
    CustomKeywords.'set.MarkAndMessage.markPassed'('Login Success')
}

